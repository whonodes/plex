name 'plex'
maintainer 'Esity'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures plex'
version '0.1.0'
chef_version '>= 13.0'

issues_url 'https://bitbucket.org/whonodes/plexissues'
source_url 'https://bitbucket.org/whonodes/plex'

%w(debian ubuntu centos redhat fedora).each do |os|
  supports os
end
