#
# Cookbook:: plex
# Recipe:: package
#
# Copyright:: 2020, The Authors, All Rights Reserved.

package 'plexmediaserver' do
  action :upgrade
  timeout 60
  notifies :restart, 'service[plexmediaserver]', :delayed
end
