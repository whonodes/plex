#
# Cookbook:: plex
# Recipe:: service
#
# Copyright:: 2020, The Authors, All Rights Reserved.

service 'plexmediaserver' do
  action [:start, :enable]
  timeout 60
end
