#
# Cookbook:: plex
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

include_recipe 'plex::repository'
include_recipe 'plex::package'
include_recipe 'plex::configure'
include_recipe 'plex::service'
