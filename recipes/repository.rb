#
# Cookbook:: plex
# Recipe:: repository
#
# Copyright:: 2020, The Authors, All Rights Reserved.

apt_repository 'plexmediaserver' do
  action :add
  components %w(public main)
  # deb https://downloads.plex.tv/repo/deb public main # working
  # deb https://downloads.plex.tv/repo/deb xenial public main # broken before
  distribution false
  uri 'https://downloads.plex.tv/repo/deb'
  key 'https://downloads.plex.tv/plex-keys/PlexSign.key'
  only_if { platform_family? 'debian' }
end

apt_repository 'plex' do
  action :remove
end