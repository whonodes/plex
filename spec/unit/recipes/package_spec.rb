#
# Cookbook:: plex
# Spec:: package
#
# Copyright:: 2020, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'plex::package' do
  context 'When all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04')
      runner.converge('plex::service', described_recipe)
    end
    let(:template) { chef_run.package('plexmediaserver') }

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    describe 'installs a package with the default action' do
      it { is_expected.to upgrade_package('plexmediaserver') }
    end

    it 'sends the specific notification to the serivce delayed' do
      expect(template).to notify('service[plexmediaserver]').to(:restart).delayed
    end
  end

  context 'When all attributes are default, on CentOS 7.4.1708' do
    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(platform: 'centos', version: '7')
      runner.converge('plex::service', described_recipe)
    end
    let(:template) { chef_run.package('plexmediaserver') }

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    describe 'installs a package with the default action' do
      it { is_expected.to upgrade_package('plexmediaserver') }
    end

    it 'sends the specific notification to the serivce delayed' do
      expect(template).to notify('service[plexmediaserver]').to(:restart).delayed
    end
  end
end
