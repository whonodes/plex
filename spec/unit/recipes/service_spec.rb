#
# Cookbook:: plex
# Spec:: service
#
# Copyright:: 2020, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'plex::service' do
  context 'When all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04')
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    describe 'enables a service with an explicit action' do
      it { is_expected.to enable_service('plexmediaserver') }
    end

    describe 'starts a service with an explicit action' do
      it { is_expected.to start_service('plexmediaserver') }
    end
  end

  context 'When all attributes are default, on CentOS 7.4.1708' do
    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(platform: 'centos', version: '7')
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    describe 'enables a service with an explicit action' do
      it { is_expected.to enable_service('plexmediaserver') }
    end

    describe 'starts a service with an explicit action' do
      it { is_expected.to start_service('plexmediaserver') }
    end
  end
end
